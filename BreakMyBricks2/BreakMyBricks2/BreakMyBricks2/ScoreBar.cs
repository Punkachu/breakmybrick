﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace BreakMyBricks2
{
    public class ScoreBar
    {
        //FIELDS           
        SpriteFont font;
        float timer;

        //CONSTRUCTOR
        public ScoreBar(ContentManager Content)
        {
            font = Content.Load<SpriteFont>("SpriteFont1");
        }
        //METHODES
        public void Initialize()
        {
            timer = 0;
        }
        //UPPDATE & DRAW
        public float Update(GameTime gametime)
        {
            timer += (float)gametime.ElapsedGameTime.TotalSeconds;
            return timer;
        }

        public void Draw(SpriteBatch spritebatch, GraphicsDeviceManager graphics)
        {
            spritebatch.DrawString(font, "Your ChronoScore is : " + timer.ToString("0.00"), new Vector2(0, graphics.PreferredBackBufferHeight - 40), Color.Black);
        }
    }
}
