﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace BreakMyBricks2
{
    public class Brick
    {
        //FIELDS
        Texture2D texture;
        Rectangle location;
        Color tint;
        bool alive;

        //PROPERTY
        public Rectangle Location
        {
            get { return location; }
        }

        //CONSTRUCTOR
        public Brick(Texture2D texture, Rectangle location, Color tint)
        {
            this.texture = texture;
            this.location = location;
            this.tint = tint;
            this.alive = true;
        }

        //METHODS
        public void CheckCollision(Ball ball)
        {
            if (alive && ball.Bounds.Intersects(location))
            {
                alive = false;
                ball.Deflection(this);
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (alive)
            {
                spriteBatch.Draw(texture, location, tint);
            }
        }
    }
}
