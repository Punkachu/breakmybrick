﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace BreakMyBricks2
{
    public class Ball
    {
        //FIELDS
        Vector2 motion;
        public Vector2 position;
        public float ballSpeed;
        Texture2D texture;
        Rectangle screenBounds;

        Rectangle bounds;
        bool collided;

        public float ballStartSpeed = 8f;
        int b;


        //PROPERTY
        public Rectangle Bounds
        {
            get
            {
                bounds.X = (int)position.X;
                bounds.Y = (int)position.Y;
                return bounds;
            }
        }

        //CONSTRUCTOR
        public Ball(ContentManager Content, Rectangle screenBounds)
        {
            texture = Content.Load<Texture2D>("Magic Trails Sparkle//magictrailssparkle_1");
            this.screenBounds = screenBounds;
            bounds = new Rectangle(0, 0, texture.Width, texture.Height);
        }

        //METHODES
        public void Initialize()
        {
            ballSpeed = 4;
        }

        public void Deflection(Brick brick)
        {
            if (!collided)
            {
                motion.Y *= -1;
                collided = true;
            }
        }

        private void CheckWallCollision()
        {
            if (position.X < 0)
            {
                position.X = 0;
                motion.X *= -1;
            }
            if (position.X + texture.Width > screenBounds.Width)
            {
                position.X = screenBounds.Width - texture.Width;
                motion.X *= -1;
            }
            if (position.Y < 0)
            {
                position.Y = 0;
                motion.Y *= -1;
            }
        }

        public void SetInStartPosition(Rectangle paddleLocation)
        {
            Random rand = new Random();
            motion = new Vector2(rand.Next(2, 6), -rand.Next(2, 6));
            motion.Normalize();
            ballSpeed = ballStartSpeed;
            position.Y = paddleLocation.Y - texture.Height;
            position.X = paddleLocation.X + (paddleLocation.Width - texture.Width) / 2;
        }

        public bool OffBottom()
        {
            if (position.Y > screenBounds.Height)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void PaddleCollision(Rectangle paddleLocation)
        {
            Rectangle ballLocation = new Rectangle(
             (int)position.X,
             (int)position.Y,
             texture.Width,
             texture.Height);
            if (paddleLocation.Intersects(ballLocation))
            {
                position.Y = paddleLocation.Y - texture.Height;
                motion.Y *= -1;
            }
        }

        //UPDATE & DRAW
        public void Update(ContentManager Content)
        {
            b++;
            if (b == 50)
            {
                b = 25; //Magic Trail Sparkle
            }
            else
            {
                texture = Content.Load<Texture2D>("Magic Trails Sparkle//magictrailssparkle_" + b);
            }

            collided = false;
            position += motion * ballSpeed;
            ballSpeed += 0.0003f;//<--------------- héhéhéhhé on va l'augmenté et ils vont souffrir !

            CheckWallCollision();
        }
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, position, Color.White);
        }
    }
}
