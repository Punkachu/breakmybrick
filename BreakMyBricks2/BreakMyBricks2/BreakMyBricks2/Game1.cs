using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace BreakMyBricks2
{
    public enum State { play, menu, GameOver, Pause };
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        public State gamestate = State.menu;
        public Texture2D menuimage;

        protected GraphicsDeviceManager graphics;
        protected SpriteBatch spriteBatch;

        protected Texture2D mouse;
        protected Rectangle mousecontainer;
        protected Vector2 mousepos = new Vector2();

        Texture2D background, GameOverTexture;
        Texture2D tempTexture;

        Paddle paddle;
        public Rectangle screenRectangle;
        int bricksWide = 21;
        int bricksHigh = 8;
        Texture2D brickImage;
        public Brick[,] bricks;
        public Ball ball;       

        //RESUME STATE
        Texture2D pausedTexture, BallP;

        //music
        SoundEffect PlayingGame;
        SoundEffectInstance playingInstance;

        //Score
        ScoreBar score;

        //GAME STRATEGY
        SpriteFont font;
        FlyingItems dreamStone;
        int ListLenght = 1;
        List<FlyingItems> ListDream = new List<FlyingItems>();

        public bool IsExit = false;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            graphics.PreferredBackBufferWidth = 1200;
            graphics.PreferredBackBufferHeight = 800;
            this.Window.Title = "Break My Bricks!";
            //MENU IMAGE
            menuimage = null;
            screenRectangle = new Rectangle(0, 0, graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight);
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
         protected override void Initialize()
        {
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            mouse = Content.Load<Texture2D>("fuckyou");

            BallP = Content.Load<Texture2D>("Stone");
            pausedTexture = Content.Load<Texture2D>("PauseMenu2");
            menuimage = Content.Load<Texture2D>("Background//wallpaper_3");
            background = Content.Load<Texture2D>("Background//wallpaper_1");
            GameOverTexture = Content.Load<Texture2D>("Background//GameOver");

            tempTexture = Content.Load<Texture2D>("Paddle2");
            paddle = new Paddle(tempTexture, screenRectangle);

            ball = new Ball(Content, screenRectangle);

            brickImage = Content.Load<Texture2D>("Blocks");

            //MUSIC STATE
            PlayingGame = Content.Load<SoundEffect>("MusicPlaying//Power");
            playingInstance = PlayingGame.CreateInstance();
            playingInstance.IsLooped = true;
            playingInstance.Volume = 0.5f;

            score = new ScoreBar(Content);
            font = Content.Load<SpriteFont>("SpriteFont1");
            dreamStone = new FlyingItems();
            dreamStone.LoadContent(Content, graphics);
            ListDream.Add(dreamStone);            
            StartGame();
        }

        public Rectangle MouseUpdate()
        {            
            mousepos.X = Mouse.GetState().X;
            mousepos.Y = Mouse.GetState().Y;
           return mousecontainer = new Rectangle((int)mousepos.X, (int)mousepos.Y, (int)mouse.Width, (int)mouse.Height);
        }

        public void StartGame()
        {
            dreamStone.Initializer();
            ball.Initialize();
            paddle.SetInStartPosition();
            ball.SetInStartPosition(paddle.GetBounds());
            bricks = new Brick[bricksWide, bricksHigh];

            for (int y = 0; y < bricksHigh; y++)
            {
                Color tint = Color.White;
                switch (y)
                {
                    case 0:
                        tint = Color.Blue;
                        break;
                    case 1:
                        tint = Color.Red;
                        break;
                    case 2:
                        tint = Color.Green;
                        break;
                    case 3:
                        tint = Color.Yellow;
                        break;
                    case 4:
                        tint = Color.Purple;
                        break;
                }
                for (int x = 0; x < bricksWide; x++)
                {
                    bricks[x, y] = new Brick(brickImage, new Rectangle(x * brickImage.Width,
                        y * brickImage.Height, brickImage.Width, brickImage.Height), tint);
                }
            }
        }


        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            /*   Texture2D tempTexture = Content.Load<Texture2D>("Paddle");
               paddle = new Paddle(tempTexture, screenRectangle);*/
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            MouseState mouse = Mouse.GetState();
            MouseUpdate();

            switch (gamestate)
            {
                //UPDATING MENU STATE
                case State.menu:
                    {
                        score.Initialize();
                        KeyboardState keystate = Keyboard.GetState();
                        if (keystate.IsKeyDown(Keys.Enter))
                        {
                            if (playingInstance.State != SoundState.Playing)
                            {
                                playingInstance.Play();
                            }
                            StartGame();
                            gamestate = State.play;
                        }
                        if (keystate.IsKeyDown(Keys.E))
                        {
                            IsExit = true;
                            Exit();
                        }
                        break;
                    }
                //UPLOADING PLAYING STATE   
                case State.play:
                    {
                        for (int i = 0; i < ListLenght; i++)
                        {
                            //dreamStone.Update(gameTime);
                            ListDream[i].Update(gameTime);
                            if (ListDream[i].MouseWipeout(ListDream[i].boundingBox,mousecontainer))
                            {
                                ListDream[i].Initializer();                                                        
                            }
                            if(ListDream[i].IsBallIntersectStone(ListDream[i].boundingBox, ball.Bounds))
                            {
                                playingInstance.Pause();
                                gamestate = State.GameOver;
                            }
                        }
                        paddle.Update();
                        ball.Update(Content);
                        score.Update(gameTime);
                        if (paddle.PaddleCollision(dreamStone.boundingBox))
                        {
                            playingInstance.Pause();
                            gamestate = State.GameOver;
                        }

                        foreach (Brick brick in bricks)
                        {
                            brick.CheckCollision(ball);
                        }

                        ball.PaddleCollision(paddle.GetBounds());
                        if (ball.OffBottom())
                        {
                            playingInstance.Pause();
                            gamestate = State.GameOver;
                        }
                        if (Keyboard.GetState().IsKeyDown(Keys.Space))
                        {
                            playingInstance.Pause();
                            gamestate = State.Pause;
                        }
                        break;
                    }
                case State.Pause:
                    {
                        KeyboardState keystate = Keyboard.GetState();
                        if (keystate.IsKeyDown(Keys.Enter))
                        {
                            playingInstance.Resume();
                            gamestate = State.play;
                        }
                        if (Keyboard.GetState().IsKeyDown(Keys.E))
                        {
                            IsExit = true;
                            Exit(); 
                        }
                        if (keystate.IsKeyDown(Keys.M))
                        {
                            playingInstance.Stop();
                            gamestate = State.menu;
                        }
                    }
                    break;
                case State.GameOver:
                    KeyboardState keystates = Keyboard.GetState();
                    if (keystates.IsKeyDown(Keys.Y))
                    {
                        score.Initialize();
                        playingInstance.Stop();
                        StartGame();
                        gamestate = State.play;
                    }
                    if (keystates.IsKeyDown(Keys.N))
                    {                      
                        playingInstance.Stop();
                        gamestate = State.menu;
                    }
                    break;
            }
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            spriteBatch.Begin();
            switch (gamestate)
            {
                case State.menu:
                    spriteBatch.Draw(menuimage, Vector2.Zero, Color.White);
                    spriteBatch.DrawString(font, "WELCOME TO BREAKMYBRICK GAME ! ", new Vector2((graphics.PreferredBackBufferWidth / 2) - 400, 80), Color.DarkRed);
                    spriteBatch.DrawString(font, "Press ENTER to start game ! ", new Vector2((graphics.PreferredBackBufferWidth / 2) - 400, 400), Color.DarkRed);
                    spriteBatch.DrawString(font, "THERE IS 1 RULE  :  ", new Vector2((graphics.PreferredBackBufferWidth / 2) - 200, 500), Color.DarkRed);
                    spriteBatch.DrawString(font, " DO  the greatest score to WIN powerfull ", new Vector2(0, 600), Color.DarkRed);
                    spriteBatch.DrawString(font, " ITEMS !!!!! ", new Vector2((graphics.PreferredBackBufferWidth / 2) - 100, 650), Color.DarkRed);
                    spriteBatch.Draw(mouse, mousepos, Color.White);
                    break;
                case State.play:
                    spriteBatch.Draw(background, screenRectangle, Color.White);
                    paddle.Draw(spriteBatch);
                    ball.Draw(spriteBatch);
                    score.Draw(spriteBatch, graphics);
                    spriteBatch.DrawString(font, "BallSpeed : " + ball.ballSpeed, new Vector2(0, graphics.PreferredBackBufferHeight - 70), Color.DarkRed);
                    for (int i = 0; i < ListLenght; i++)
                    {
                        ListDream[i].Draw(spriteBatch);
                    }

                    foreach (Brick brick in bricks)
                    {
                        brick.Draw(spriteBatch);
                    }                    
                    spriteBatch.Draw(mouse, mousepos, Color.White);
                    break;
                case State.Pause:
                    spriteBatch.Draw(background, screenRectangle, Color.White);
                    paddle.Draw(spriteBatch);
                    ball.Draw(spriteBatch);
                    score.Draw(spriteBatch, graphics);
                    spriteBatch.DrawString(font, "BallSpeed : " + ball.ballSpeed, new Vector2(0, graphics.PreferredBackBufferHeight - 70), Color.DarkRed);
                    for (int i = 0; i < ListLenght; i++)
                    {
                        ListDream[i].Draw(spriteBatch);
                    }

                    foreach (Brick brick in bricks)
                    {
                        brick.Draw(spriteBatch);
                    }
                    spriteBatch.Draw(pausedTexture, screenRectangle, Color.White);
                    spriteBatch.Draw(BallP, ball.position, Color.White);
                    spriteBatch.DrawString(font, "Press  ENTER  to  resume", new Vector2(100, 150), Color.White);
                    spriteBatch.DrawString(font, "Press  E  to  Exit", new Vector2(100, 250), Color.White);
                    spriteBatch.DrawString(font, "Press  M  to  Come  Back  to  Menu", new Vector2(100, 350), Color.White);
                    spriteBatch.Draw(mouse, mousepos, Color.White);
                    break;
                case State.GameOver:                    
                    spriteBatch.Draw(GameOverTexture, screenRectangle, Color.White);
                    spriteBatch.DrawString(font, "WOULD  YOU   LIKE TO  CONTINUE ? " , new Vector2((graphics.PreferredBackBufferWidth/2)-300,80), Color.DarkRed);
                    spriteBatch.DrawString(font, "Press  Y  to  Continue", new Vector2(40, 600), Color.DarkRed);
                    spriteBatch.DrawString(font, "Press  N  to  Come Back to Menu", new Vector2(40, 650), Color.DarkRed);
                    spriteBatch.Draw(mouse, mousepos, Color.White);
                    break;
            }

            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
