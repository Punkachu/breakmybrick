﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Timers;

namespace BreakMyBricks2
{
    public class FlyingItems
    {
        public Rectangle boundingBox;
        public Texture2D texture;
        public Vector2 position;
        public Vector2 origine;
        public float rotationAngle;
        public int speed;
        public bool isColliding, destroyed;
        public bool IsAlive;
        Random random = new Random();
        

        public FlyingItems()
        {
            position = new Vector2(400, -50);
            texture = null;
            speed = 4;
            isColliding = false;
            IsAlive = true;
            destroyed = false;
        }
        //METHODS
        public void Initializer()
        {
            position = new Vector2(400, -50);
        }

        public void LoadContent(ContentManager Content, GraphicsDeviceManager graphics)
        {
            texture = Content.Load<Texture2D>("Dream");
            origine.X = texture.Width  - 200;
            origine.Y = texture.Height - 200;
        }

        public bool MouseWipeout(Rectangle FlyerStone, Rectangle mousecontainer)
        {            
            if(mousecontainer.Intersects(FlyerStone) &
                Mouse.GetState().LeftButton == ButtonState.Pressed)
            {
                return IsAlive;
            }
            else
            {
                return ! IsAlive;
            }
        }

        public bool IsBallIntersectStone(Rectangle FlyerSTone, Rectangle ball)
        {
            if(ball.Intersects(FlyerSTone))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //UPDATE & DRAW
        public void Update(GameTime gametime)
        {
            //set BoundingBox for Collision with Mouse or paddle...
            boundingBox = new Rectangle((int)position.X, (int)position.Y, texture.Width, texture.Height);

            //UPDATE MOVEMENT
            int rndMin = 0 + texture.Width;
            int rndMax =﻿ 1200 - texture.Width;
            Random rnd = new Random();
            int rand = rnd.Next(rndMin, rndMax);

            position.Y = position.Y + speed;

            if (position.Y >= 800)
            {
                position.X = rand;
                position.Y = -50;
            }
            /*    //Rotate Items      la flemme au final Tn T
                float elapsed = (float)gametime.ElapsedGameTime.TotalSeconds;
                rotationAngle += elapsed;
                float circle = MathHelper.Pi * 2;
                rotationAngle = rotationAngle % circle;  */
        }

        public void Draw(SpriteBatch spritebatch)
        {
            if (!destroyed && IsAlive)
            {
                // spritebatch.Draw(texture, position, null, Color.White, rotationAngle, origine, 1.0f, SpriteEffects.None, 0f);
                spritebatch.Draw(texture, position, Color.White);
            }
        }
    }
}
